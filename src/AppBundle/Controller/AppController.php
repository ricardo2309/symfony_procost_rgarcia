<?php

namespace AppBundle\Controller;

use BackOfficeBundle\Entity\Employee;
use BackOfficeBundle\Entity\Employee_Job;
use BackOfficeBundle\Entity\Production_Time;
use BackOfficeBundle\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AppController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $employee_list = $this->getDoctrine()->getRepository(Employee::class)->findAll();
        $delivered_projects = $this->getDoctrine()->getRepository(Project::class)->findByStatus(1);
        $projects_in_progress = $this->getDoctrine()->getRepository(Project::class)->findByStatus(0);
        $production_times = $this->getDoctrine()->getRepository(Production_Time::class)->findAll();
        $recent_production_times = $this->getDoctrine()->getRepository(Production_Time::class)->findBy([],[],8);
        $project_list = $this->getDoctrine()->getRepository(Project::class)->findAll();
        $projects_capex = $this->getDoctrine()->getRepository(Project::class)->findByType('CAPEX');
        $projects_opex = $this->getDoctrine()->getRepository(Project::class)->findByType('OPEX');

        // Totals
        $total_days = 0;
        $total_projecs_not_delivered = (sizeof($projects_in_progress) * 100 / sizeof($project_list));
        $total_projects_delivered = (sizeof($delivered_projects) * 100 / sizeof($project_list));
        $total_projects_capex = (sizeof($projects_capex) * 100 / sizeof($project_list));
        $total_projects_opex = (sizeof($projects_opex) * 100 / sizeof($project_list));

        $paginator = $this->get('knp_paginator');
        $total_projects = $paginator->paginate(
            $project_list,
            $request->query->getInt('page',1),
            $request->query->getInt('limit', 5)
        );

        foreach ($production_times as $production_time)
        {
            $total_days += $production_time->getDaysWorked();
        }
        $data_bundle = [
            'employee_list'                => $employee_list,
            'delivered_projects'           => $delivered_projects,
            'projects_in_progress'         => $projects_in_progress,
            'production_times'             => $production_times,
            'recent_production_times'      => $recent_production_times,
            'project_list'                 => $total_projects,
            'project_capex_list'           => $projects_capex,
            'total_days'                   => $total_days,
            'total_projects_capex'         => $total_projects_capex,
            'total_projects_opex'          => $total_projects_opex,
            'total_projects_delivered'     => $total_projects_delivered,
            'total_projects_not_delivered' => $total_projecs_not_delivered,
        ];
        return $this->render('app/index.html.twig', $data_bundle);
    }

    /**
     * @Route("employees", name="back_employees")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function employeeAction(Request $request )
    {
        $employee_list = $this->getDoctrine()->getRepository(Employee::class)->findAll();
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $employee_list,
            $request->query->getInt('page',1),
            $request->query->getInt('limit', 10)
        );
        $data_bundle = ['employee_list' => $result];
        return $this->render('backoffice/employee/employee-list.html.twig', $data_bundle);
    }

    /**
     * @Route("projects", name="back_projects")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function projectAction(Request $request)
    {
        $project_list = $this->getDoctrine()->getRepository(Project::class)->findAll();
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $project_list,
            $request->query->getInt('page',1),
            $request->query->getInt('limit', 10)
        );
        $data_bundle = ['project_list' => $result];
        return $this->render('backoffice/project/project-list.html.twig', $data_bundle);
    }

    /**
     * @Route("jobs", name="back_jobs")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function jobAction(Request $request)
    {
        $job_list = $this->getDoctrine()->getRepository(Employee_Job::class)->findAll();
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $job_list,
            $request->query->getInt('page',1),
            $request->query->getInt('limit', 10)
        );
        $data_bundle = ['job_list' => $result];
        return $this->render('backoffice/job/job-list.html.twig', $data_bundle);
    }
}

