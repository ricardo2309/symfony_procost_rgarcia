<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 20/04/2018
 * Time: 03:32 PM
 */

namespace BackOfficeBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class JobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder->add('description', TextType::class, ['label' => 'Métier']);
    }

    public function getName()
    {
        return 'job_form';
    }
}