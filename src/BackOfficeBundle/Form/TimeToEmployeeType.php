<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 29/04/2018
 * Time: 07:31 PM
 */

namespace BackOfficeBundle\Form;

use BackOfficeBundle\Entity\Employee;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class TimeToEmployeeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('days_worked', NumberType::class, [
            'label' => 'Nombre de jours',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'col-sm-2 col-sm-2 control-label'
            ]
        ]);
        $builder->add('employee',EntityType::class,[
            'class' => Employee::class,
            'choice_label' => function (Employee $employee) {
                return $employee->getName() . ' ' . $employee->getLastName();
            },
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('e')
                    ->where('e.status=1');
            },
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'col-sm-2 col-sm-2 control-label'
            ],
            'label' => 'Employé'
        ]);
    }

    public function getName()
    {
        return 'time_to_employee_form';
    }

}