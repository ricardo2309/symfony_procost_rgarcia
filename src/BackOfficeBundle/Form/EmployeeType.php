<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 22/04/2018
 * Time: 09:04 PM
 */

namespace BackOfficeBundle\Form;

use BackOfficeBundle\Entity\Employee_Job;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EmployeeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Prénom',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('lastName', TextType::class,[
            'label' => 'Nom',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('email', EmailType::class,[
            'label' => 'Courrier éléctronique',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('hiringDate', DateType::class, [
            'label' => 'Date d\'embauche',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('dailyCost', NumberType::class, [
            'label' => 'Coût Journalier ',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('employee_job',EntityType::class,[
            'class' => Employee_Job::class,
            'choice_label' => function (Employee_Job $employee_Job) {
                return $employee_Job->getDescription();
            },
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ],
            'label' => 'Métier'
        ]);
    }

    public function getName()
    {
        return 'employee_form';
    }

}