<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 28/04/2018
 * Time: 06:32 PM
 */

namespace BackOfficeBundle\Form;


use BackOfficeBundle\Entity\Project;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TimeToProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('days_worked', NumberType::class, [
            'label' => 'Nombre de jours',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'col-sm-2 col-sm-2 control-label'
            ]
        ]);
        $builder->add('project',EntityType::class,[
            'class' => Project::class,
            'choice_label' => function (Project $project) {
                return $project->getTitle();
            },
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->where('p.deliveryStatus=0');
            },
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'col-sm-2 col-sm-2 control-label'
            ],
            'label' => 'Projet à alimenter'
        ]);
    }

    public function getName()
    {
        return 'time_to_project_form';
    }
}