<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 29/04/2018
 * Time: 01:08 PM
 */

namespace BackOfficeBundle\Form;

use BackOfficeBundle\Entity\Project_Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ProjectTypeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Titre du project',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('description', TextareaType::class, [
            'label' => 'Description',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('creationDate', DateType::class, [
            'label' => 'Date de création',
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ]
        ]);
        $builder->add('project_type',EntityType::class,[
            'class' => Project_Type::class,
            'choice_label' => function (Project_Type $project_Type) {
                return $project_Type->getDescription();
            },
            'attr' => [
                'class' => 'form-control'
            ],
            'label_attr' => [
                'class' => 'control-label'
            ],
            'label' => 'Type du projet'
        ]);
    }

    public function getName()
    {
        return 'project_form';
    }

}