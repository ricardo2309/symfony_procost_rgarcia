<?php

namespace BackOfficeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Employee
 *
 * @ORM\Table(name="employee")
 * @ORM\Entity(repositoryClass="BackOfficeBundle\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="BackOfficeBundle\Entity\Employee_Job")
     */
    private $employee_job;

    /**
     * @ORM\OneToMany(targetEntity="BackOfficeBundle\Entity\Production_Time", mappedBy="employee", cascade={"persist"})
     */
    private $production_time;

    /**
     * @var int
     *
     * @ORM\Column(name="daily_cost", type="decimal",precision=7, scale=2)
     */
    private $dailyCost;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hiring_date", type="date")
     */
    private $hiringDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active_status", type="boolean")
     */
    private $status;

    public function __construct()
    {
        $this->production_time = new ArrayCollection();
        $this->status = true;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Employee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Employee
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Employee
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getEmployeeJob()
    {
        return $this->employee_job;
    }

    /**
     * @param mixed $employee_job
     */
    public function setEmployeeJob(Employee_Job $employee_job)
    {
        $this->employee_job = $employee_job;
    }

    public function addProductionTime(Production_Time $production_Time)
    {
        $this->production_time[] = $production_Time;
    }

    public  function removeProductionTime(Production_Time $production_Time)
    {
        $this->production_time->removeElement($production_Time);
    }

    /**.
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getProductionTime()
    {
        return $this->production_time;
    }

    /**
     * Set dailyCost
     *
     * @param integer $dailyCost
     *
     * @return Employee
     */
    public function setDailyCost($dailyCost)
    {
        $this->dailyCost = $dailyCost;

        return $this;
    }

    /**
     * Get dailyCost
     *
     * @return int
     */
    public function getDailyCost()
    {
        return $this->dailyCost;
    }

    /**
     * Set hiringDate
     *
     * @param \DateTime $hiringDate
     *
     * @return Employee
     */
    public function setHiringDate($hiringDate)
    {
        $this->hiringDate = $hiringDate;

        return $this;
    }

    /**
     * Get hiringDate
     *
     * @return \DateTime
     */
    public function getHiringDate()
    {
        return $this->hiringDate;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


}

