<?php

namespace BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Production_Time
 *
 * @ORM\Table(name="production__time")
 * @ORM\Entity(repositoryClass="BackOfficeBundle\Repository\Production_TimeRepository")
 */
class Production_Time
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BackOfficeBundle\Entity\Employee", inversedBy="production_time")
     */
    private $employee;

    /**
     * @ORM\ManyToOne(targetEntity="BackOfficeBundle\Entity\Project", inversedBy="production_time")
     */
    private $project;

    /**
     * @var int
     *
     * @ORM\Column(name="days_worked", type="integer")
     */
    private $daysWorked;

    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="decimal", precision=7, scale=2)
     */
    private $cost;

    /**
     * @var \DateTime
     * @ORM\Column(name="registered_date", type="date")
     */
    private $registeredDate;

    public function __construct()
    {
        $this->registeredDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set daysWorked
     *
     * @param integer $daysWorked
     *
     * @return Production_Time
     */
    public function setDaysWorked($daysWorked)
    {
        $this->daysWorked = $daysWorked;

        return $this;
    }

    /**
     * Get daysWorked
     *
     * @return int
     */
    public function getDaysWorked()
    {
        return $this->daysWorked;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }



    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return Production_Time
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredDate(): \DateTime
    {
        return $this->registeredDate;
    }

    /**
     * @param \DateTime $registeredDate
     */
    public function setRegisteredDate(\DateTime $registeredDate)
    {
        $this->registeredDate = $registeredDate;
    }
}

