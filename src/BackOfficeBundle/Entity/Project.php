<?php

namespace BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="BackOfficeBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Le champs ne peut pas être vide")
     * @Assert\Length(min=5, max=50, minMessage="La longeur doit être de {{limit}} caractères au minimum", maxMessage="La longeur doit être de {{limit}} caractères au maximum")
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="Le champs ne peut pas être vide")
     * @Assert\Length(min=5, max=500, minMessage="La longeur doit être de {{limit}} caractères au minimum", maxMessage="La longeur doit être de {{limit}} caractères au maximum")
     * @ORM\Column(name="description", type="string", length=500)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="BackOfficeBundle\Entity\Project_Type")
     */
    private $project_type;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\Column(name="total_cost", type="decimal",precision=7, scale=2)
     */
    private $total_cost;

    /**
     * @ORM\OneToMany(targetEntity="BackOfficeBundle\Entity\Production_Time", mappedBy="project", cascade={"persist"})
     */
    private $production_time;

    /**
     * @var \DateTime
     *@Assert\Date()
     * @ORM\Column(name="creation_date", type="date")
     */
    private $creationDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="delivery_status", type="boolean")
     */
    private $deliveryStatus;


    public function __construct()
    {
        $this->creationDate = new \DateTime();
        $this->total_cost = 0;
        $this->deliveryStatus = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    public function getProjectType()
    {
        return $this->project_type;
    }

    public function setProjectType(Project_Type $project_Type)
    {
        $this->project_type = $project_Type;
    }

    /**
     * @return int
     */
    public function getTotalCost(): int
    {
        return $this->total_cost;
    }

    /**
     * @param int $cost
     */
    public function addCost(int $cost)
    {
        $this->total_cost = $this->total_cost + $cost;
    }

    /**
     * @param int $cost
     */
    public function discountCost(int $cost)
    {
        $this->total_cost = $this->total_cost - $cost;
    }


    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Project
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set deliveryStatus
     *
     * @param boolean $deliveryStatus
     *
     * @return Project
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return bool
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * @return mixed
     */
    public function getProductionTime()
    {
        return $this->production_time;
    }

    /**
     * @param mixed $production_time
     */
    public function setProductionTime($production_time)
    {
        $this->production_time = $production_time;
    }

}

