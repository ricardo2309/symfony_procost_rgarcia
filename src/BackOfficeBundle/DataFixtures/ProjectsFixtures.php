<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 18/04/2018
 * Time: 05:59 PM
 */

namespace BackOfficeBundle\DataFixtures;

use BackOfficeBundle\Entity\Project;
use BackOfficeBundle\Entity\Project_Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProjectsFixtures extends Fixture
{


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $project_type_names = [
            'CAPEX',
            'OPEX'
        ];
        $project_types = [];

        for($j = 0; $j < sizeof($project_type_names); $j ++)
        {
            $pt = new Project_Type();
            $pt->setDescription($project_type_names[$j]);
            $manager->persist($pt);
            $project_types[] = $pt;
        }
        $manager->flush();

        for($i = 0; $i < 5; $i ++)
        {
            $project = new Project();
            $project_type = $project_types[mt_rand(0,1)];
            $project->setProjectType($project_type);
            $project->setTitle("Project $i");
            $project->setDescription("This is a little description about Project $i");
            $project->setCreationDate(new \DateTime());
            $project->setDeliveryStatus(false);
            $manager->persist($project);
        }
        $manager->flush();
    }
}