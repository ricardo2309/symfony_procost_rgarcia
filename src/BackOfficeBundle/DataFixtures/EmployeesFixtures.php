<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 21/04/2018
 * Time: 07:56 PM
 */

namespace BackOfficeBundle\DataFixtures;

use BackOfficeBundle\Entity\Employee_Job;
use BackOfficeBundle\Entity\Employee;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EmployeesFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $job_list = [];
        for($i = 1; $i <= 5; $i ++)
        {
            $employee_job = new Employee_Job();
            $employee_job->setDescription("Métier $i");
            $manager->persist($employee_job);
            $job_list[] = $employee_job;
        }
        for ($index = 1; $index <= 20; $index ++)
        {
            $employee = new Employee();
            $employee->setName("Employee $index");
            $employee->setLastName("Lastname $index");
            $employee->setDailyCost(rand(80,250));
            $employee->setEmail("employee$index@mail.com");
            $employee->setHiringDate(new \DateTime());
            $employee->setEmployeeJob($job_list[mt_rand(0,4)]);
            $manager->persist($employee);
        }
        $manager->flush();
    }
}