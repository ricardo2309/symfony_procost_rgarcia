<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 29/04/2018
 * Time: 01:06 PM
 */

namespace BackOfficeBundle\Controller;


use BackOfficeBundle\Entity\Employee;
use BackOfficeBundle\Entity\Production_Time;
use BackOfficeBundle\Entity\Project;
use BackOfficeBundle\Form\ProjectTypeForm;
use BackOfficeBundle\Form\TimeToEmployeeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProjectController extends Controller
{

    /**
     * @Route("/projects/new",name="back_insert_project")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function insertProjectAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $project = new Project();
        $form = $this->createForm(ProjectTypeForm::class, $project);

        $data_bundle = [
            'project'  => $project,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_insert_project',
                'method' => 'POST'
            ],
            'view_settings' => [
                'title' => 'PROCOST - Ajouter un projet',
                'header_title' => 'Ajouter un projet'
            ]
        ];

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                try {
                    $em->persist($project);
                    $em->flush();
                    return $this->redirect($this->generateUrl('back_projects'));
                } catch (\Exception $exception) {
                    $data_bundle['error'] = $exception->getMessage();
                }
            }
        }
        return $this->render('backoffice/project/project-form.html.twig', $data_bundle);
    }

    /**
     * @Route("/projects/edit/{id}", name="back_edit_project", requirements={"id"="\d+"})
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateProjectAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository(Project::class)->find($id);
        $form = $this->createForm(ProjectTypeForm::class, $project);
        if (is_null($project))
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
        $data_bundle = [
            'project'  => $project,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_edit_project',
                'method' => 'POST'
            ],
            'view_settings' => [
                'title' => 'PROCOST - Modifier un projet',
                'header_title' => 'Modifier un projet'
            ]
        ];

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                try {
                    $em->persist($project);
                    $em->flush();
                    return $this->redirect($this->generateUrl('back_projects'));
                } catch (\Exception $exception) {
                    $data_bundle['error'] = $exception->getMessage();
                }
            }
        }

        return $this->render('backoffice/project/project-form.html.twig', $data_bundle);
    }

    /**
     * @Route("/projects/timetraking/{id}", name="back_timetracking_project", requirements={"id"="\d+"})
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productionTimeProjectAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var \BackOfficeBundle\Entity\Project
         */
        $project = $em->getRepository(Project::class)->find($id);
        if (is_null($project))
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
        $production_time = new Production_Time();
        $form = $this->createForm(TimeToEmployeeType::class, $production_time);
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $project->getProductionTime(),
            $request->query->getInt('page',1),
            $request->query->getInt('limit', 5)
        );

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $employee = $form->get('employee')->getData();
                $production_time->setEmployee($employee);
                $production_time->setProject($project);
                $production_time->setCost($production_time->getDaysWorked() * $employee->getDailyCost());
                return $this->insertProductionTime($employee, $project, $production_time);
            }
        }
        $data_bundle = [
            'project' => $project,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_insert_timetracking_project',
                'method' => 'POST'
            ],
            'production_time_list' => $result
        ];
        return $this->render('backoffice/project/project-detail.html.twig', $data_bundle);
    }

    /**
     * @param Employee $employee
     * @param Project $project
     * @param Production_Time $production_time
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function insertProductionTime(Employee $employee, Project $project, Production_Time $production_time)
    {
        $em = $this->getDoctrine()->getManager();
        $project->addCost($production_time->getCost());
        $employee->addProductionTime($production_time);
        $em->persist($employee);
        $em->persist($production_time);
        $em->persist($project);
        $em->flush();
        return $this->redirect($this->generateUrl('back_timetracking_project',['id' => $project->getId()]));
    }

    /**
     * @Route("/projects/delete/{id}", name="back_delete_project", requirements={"id"="\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteProjectAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($id);
        try
        {
            $em->remove($project);
            $em->flush();
            return $this->redirect($this->generateUrl('back_projects'));
        } catch (\Exception $exception)
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
    }

    /**
     * @Route("/projects/deliver/{id}", name="back_deliver_project", requirements={"id"="\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deliverProjectAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($id);
        $project->setDeliveryStatus(true);
        $em->persist($project);
        $em->flush();
        return $this->redirect($this->generateUrl('back_projects'));
    }

}