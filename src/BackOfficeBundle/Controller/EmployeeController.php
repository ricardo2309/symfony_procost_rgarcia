<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 22/04/2018
 * Time: 01:25 AM
 */

namespace BackOfficeBundle\Controller;

use BackOfficeBundle\Entity\Employee;
use BackOfficeBundle\Form\EmployeeType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class EmployeeController extends Controller
{

    /**
     * @Route("employees/new", name="back_insert_employee")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function insertEmployeeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);

        $data_bundle = [
            'employee'  => $employee,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_insert_employee',
                'method' => 'POST'
            ],
            'view_settings' => [
                'title' => 'PROCOST - Ajouter un employee',
                'header_title' => 'Ajouter un employee'
            ]
        ];

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                try {
                    $em->persist($employee);
                    $em->flush();
                    return $this->redirect($this->generateUrl('back_employees'));
                } catch (\Exception $exception) {
                    $data_bundle['error'] = $exception->getMessage();
                }
            }
        }

        return $this->render('backoffice/employee/employee-form.html.twig', $data_bundle);
    }


    /**
     * @Route("employees/{id}", name="back_edit_employee", requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateEmployeeAction(Request $request, int $id)
    {
        $em = $this->getDoctrine()->getManager();

        $employee = $em->getRepository(Employee::class)->find($id);
        $form = $this->createForm(EmployeeType::class, $employee);
        if (is_null($employee))
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
        $data_bundle = [
            'employee'  => $employee,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_edit_employee',
                'method' => 'POST'
            ],
            'view_settings' => [
                'title' => 'PROCOST - Modifier un employee',
                'header_title' => 'Modifier un employee'
            ]
        ];

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                try {
                    $em->persist($employee);
                    $em->flush();
                    return $this->redirect($this->generateUrl('back_employees'));
                } catch (\Exception $exception) {
                    $data_bundle['error'] = $exception->getMessage();
                }
            }
        }

        return $this->render('backoffice/employee/employee-form.html.twig', $data_bundle);
    }


    /**
     * @Route("/employees/archiver/{id}", name="back_disable_employee", requirements={"id"="\d+"})
     * @param $id int
     *        Identifiant de l'employé
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function disableEmployeeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var $job \BackOfficeBundle\Entity\Employee
         */
        $job = $em->getRepository(Employee::class)->find($id);
        try
        {
            $job->setStatus($job->getStatus() ? false : true);
            $em->persist($job);
            $em->flush();
            return $this->redirect($this->generateUrl('back_employees'));
        } catch (\Exception $exception)
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
    }
}