<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 21/04/2018
 * Time: 03:18 PM
 */

namespace BackOfficeBundle\Controller;

use BackOfficeBundle\Entity\Employee_Job;
use BackOfficeBundle\Form\JobType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class JobController extends Controller
{

    /**
     * @Route("jobs/new", name="back_insert_jobs")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function jobInsertAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $job = new Employee_Job();
        $form = $this->createForm(JobType::class, $job);

        $data_bundle = [
            'job'  => $job,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_insert_jobs',
                'method' => 'POST'
            ],
            'view_settings' => [
                'title' => 'PROCOST - Ajouter un métier',
                'header_title' => 'Ajouter un métier'
            ]
        ];

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                try {
                    $em->persist($job);
                    $em->flush();
                    return $this->redirect($this->generateUrl('back_jobs'));
                } catch (\Exception $exception) {
                    $data_bundle['error'] = 'Le nom du métier est déjà enregistré';
                }
            }
        }
        return $this->render('backoffice/job/job-form.html.twig', $data_bundle);
    }

    /**
     * @Route("jobs/{id}", name="back_edit_jobs", requirements={"id"="\d+"})
     * @param int $id
     *      Job id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function jobUpdateAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $job = $em->getRepository(Employee_Job::class)->find($id);
        if (is_null($job))
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }

        $form = $this->createForm(JobType::class, $job);
        $data_bundle = [
            'job'  => $job,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_edit_jobs',
                'method' => 'POST'
            ],
            'view_settings' => [
                'title' => 'PROCOST - Edition d\'un métier',
                'header_title' => 'Edition d\'un métier'
            ]
        ];

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                try {
                    $em->persist($job);
                    $em->flush();
                    return $this->redirect($this->generateUrl('back_jobs'));
                } catch (\Exception $exception) {
                    $data_bundle['error'] = 'Le nom du métier est déjà enregistré';
                }
            }
        }

        return $this->render('backoffice/job/job-form.html.twig', $data_bundle);
    }

    /**
     * @Route("jobs/delete/{id}", name="back_state_jobs", requirements={"id"="\d+"})
     * @param int $id
     *      Job id
     * @return object
     */
    public function jobDeleteAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository(Employee_Job::class)->find($id);
        try
        {
            $em->remove($job);
            $em->flush();
            return $this->redirect($this->generateUrl('back_jobs'));
        } catch (\Exception $exception)
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
    }

}