<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ricardo GarcÃ­a
 * Date: 28/04/2018
 * Time: 09:45 PM
 */

namespace BackOfficeBundle\Controller;

use BackOfficeBundle\Entity\Employee;
use BackOfficeBundle\Entity\Production_Time;
use BackOfficeBundle\Entity\Project;
use BackOfficeBundle\Form\TimeToProjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ProductionTimeController extends Controller
{

    /**
     * @Route("/employees/timetracking/{id}", name="back_timetracking_employee", requirements={"id"="\d+"})
     * @param $id int
     *        Identifiant de l'employé
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productionTimeEmployeeAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var \BackOfficeBundle\Entity\Employee
         */
        $employee = $em->getRepository(Employee::class)->find($id);
        if (is_null($employee))
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
        $production_time = new Production_Time();
        $form = $this->createForm(TimeToProjectType::class, $production_time);
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $employee->getProductionTime(),
            $request->query->getInt('page',1),
            $request->query->getInt('limit', 5)
        );

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $project = $form->get('project')->getData();
                $production_time->setEmployee($employee);
                $production_time->setCost($production_time->getDaysWorked() * $employee->getDailyCost());
                return $this->insertProductionTime($employee, $project, $production_time);
            }
        }
        $data_bundle = [
            'employee' => $employee,
            'form' => $form->createView(),
            'form_settings' => [
                'action' => 'back_insert_timetracking_employee',
                'method' => 'POST'
            ],
            'production_time_list' => $result
        ];
        return $this->render('backoffice/employee/employee-detail.html.twig', $data_bundle);
    }

    /**
     * @param Employee $employee
     * @param Project $project
     * @param Production_Time $production_time
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function insertProductionTime(Employee $employee, Project $project, Production_Time $production_time)
    {
        $em = $this->getDoctrine()->getManager();
        $project->addCost($production_time->getCost());
        $employee->addProductionTime($production_time);
        $em->persist($employee);
        $em->persist($production_time);
        $em->persist($project);
        $em->flush();
        return $this->redirect($this->generateUrl('back_timetracking_employee',['id' => $employee->getId()]));
    }

    /**
     * @Route("/employee/delete/production_time/{id}/employee/{id_em}/project/{id_proj}", name="back_delete_production_time", requirements={"id"="\d+","id_em"="\d+","id_proj"="\d+"})
     * @param int $id
     * @param int $id_em
     * @param int $id_proj
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteProductionTimeAction(int $id, int $id_em, int $id_proj)
    {
        $em = $this->getDoctrine()->getManager();
        $production_time = $em->getRepository(Production_Time::class)->find($id);
        $project = $em->getRepository(Project::class)->find($id_proj);
        $project->discountCost($production_time->getcost());
        try
        {
            $em->remove($production_time);
            $em->persist($project);
            $em->flush();
            return $this->redirect($this->generateUrl('back_timetracking_employee',['id' =>$id_em]));
        } catch (\Exception $exception)
        {
            $data_bundle = [
                'view_settings' => [
                    'title' => 'Page non trouvée'
                ]
            ];
            return $this->render('error/error404.html.twig', $data_bundle);
        }
    }

}