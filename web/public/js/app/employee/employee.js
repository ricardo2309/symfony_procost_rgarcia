function statusEmployee(id_job, status) {

    swal({
        title: status == 1 ? 'Archiver l\'employé ?' : 'Réactiver l\'employé ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: status == 1 ? 'Archiver' : 'Réactiver',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.value) {
        window.location.replace("http://localhost:8000/employees/archiver/" + id_job);
    }
});

}