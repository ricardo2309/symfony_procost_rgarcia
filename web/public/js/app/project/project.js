function deliverProject(id_project) {

    swal({
        title: 'Livrer le projet ?',
        text: 'La saisie du temps de production et le formulaire de modification ne seront plus accessible',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Livrer',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.value) {
        window.location.replace("http://localhost:8000/projects/deliver/"+ id_project);

    }
})
}

function deleteProject(id_project) {

    swal({
        title: 'Supprimer le projet ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Supprimer',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.value) {
        window.location.replace("http://localhost:8000/projects/delete/"+ id_project);

    }
})

}