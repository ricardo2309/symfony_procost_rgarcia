function deleteProductionTime(id_prod, id_employee, id_project) {

    swal({
        title: 'Supprimer le temps de production ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Supprimer',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.value) {
        window.location.replace("http://localhost:8000/employee/delete/production_time/"+ id_prod + "/employee/" + id_employee + "/project/" + id_project);

    }
})

}