function deleteJob(id_job) {

    swal({
        title: 'Supprimer le métier ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Supprimer',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.value) {
            window.location.replace("http://localhost:8000/jobs/delete/"+ id_job);

        }
    })

}